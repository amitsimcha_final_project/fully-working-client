﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace loadScrean
{
    public partial class Scores : Form
    {
        // a list thet contains all the user names thet were in the game and their scores  
        private List<string> _usersNamesAndScores = new List<string>();

        public Scores(List<string> usersNamesAndScores)
        {
            _usersNamesAndScores = usersNamesAndScores;

            InitializeComponent();

            UserNamesAndScoresLabel.Text = "";
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        private void Scores_Load(object sender, EventArgs e)
        {
            for(int i = 0; i < _usersNamesAndScores.Count; i += 2)
            {
                string temp = "";

                temp = "User name: " + _usersNamesAndScores[i] + "  Score:  " + _usersNamesAndScores[i + 1];
                UserNamesAndScoresLabel.Text += (temp + "\n");
            }
        }
    }
}
