﻿namespace loadScrean
{
    partial class Scores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OkButton = new System.Windows.Forms.Button();
            this.UserNamesAndScoresLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // OkButton
            // 
            this.OkButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.OkButton.Location = new System.Drawing.Point(42, 252);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(225, 33);
            this.OkButton.TabIndex = 0;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // UserNamesAndScoresLabel
            // 
            this.UserNamesAndScoresLabel.AutoSize = true;
            this.UserNamesAndScoresLabel.BackColor = System.Drawing.Color.Coral;
            this.UserNamesAndScoresLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10F);
            this.UserNamesAndScoresLabel.ForeColor = System.Drawing.Color.Snow;
            this.UserNamesAndScoresLabel.Location = new System.Drawing.Point(39, 36);
            this.UserNamesAndScoresLabel.Name = "UserNamesAndScoresLabel";
            this.UserNamesAndScoresLabel.Size = new System.Drawing.Size(92, 16);
            this.UserNamesAndScoresLabel.TabIndex = 1;
            this.UserNamesAndScoresLabel.Text = "ScoresLabel";
            // 
            // Scores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::loadScrean.Properties.Resources.blue_binary;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(312, 297);
            this.Controls.Add(this.UserNamesAndScoresLabel);
            this.Controls.Add(this.OkButton);
            this.Name = "Scores";
            this.Text = "Scores";
            this.Load += new System.EventHandler(this.Scores_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Label UserNamesAndScoresLabel;
    }
}